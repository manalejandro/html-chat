document.addEventListener('DOMContentLoaded', function () {
    if (!window.location.hash || window.location.hash === '#') {
        window.location.hash = window.prompt('Who are you?') || ''
    }
    while (!window.location.hash.substr(1)
        || window.location.hash.substr(1) === 'null'
        || !window.location.hash.substr(1).match(/^[a-zA-Z]{1,10}$/))
        window.location.hash = window.prompt('Error: username must be [a-zA-Z] 10 char max.') || ''
    var me = {},
        socket = new window.WebSocket('wss://' + window.location.host)
    me.name = window.location.hash.substr(1)
    function isJSON(str) {
        try {
            JSON.parse(str)
        } catch (e) {
            return false
        }
        return true
    }
    socket.addEventListener('open', function (event) {
        var client = new window.ClientJS(),
            properties = Object.getOwnPropertyNames(ClientJS.super_.prototype).reduce((acc, val) => {
                if (typeof client[val] === 'function') {
                    return { ...acc, [val]: client[val].call(client) }
                }
            }, {})
        me.id = client.getFingerprint()
        socket.send(JSON.stringify({
            name: me.name,
            id: me.id,
            properties
        }))
        document.querySelector('label[for="box"]').innerHTML = '<strong>' + me.name + '</strong>:'
        setInterval(() => {
            if (socket.readyState === window.WebSocket.OPEN) {
                socket.send(JSON.stringify({ ping: Date.now() }))
            }
        }, 5000)
    })
    socket.addEventListener('message', function (event) {
        if (isJSON(event.data) && typeof JSON.parse(event.data).pong === 'number') {
        } else if (isJSON(event.data) && typeof JSON.parse(event.data).userlist === 'object') {
            var data = JSON.parse(event.data)
            document.getElementById('userlist').innerHTML = ''
            data.userlist.map(function (user) {
                var li = document.createElement('li'),
                    text = document.createTextNode(user)
                li.appendChild(text)
                document.getElementById('userlist').appendChild(li)
            })
        } else if (isJSON(event.data) && typeof JSON.parse(event.data).message === 'string') {
            var msg = JSON.parse(event.data),
                date = new Date(msg.date)
            document.getElementById('chat').innerText += `[${date.getHours().length < 2 ? '0' : ''}${date.getHours()}:${date.getMinutes().length < 2 ? '0' : ''}${date.getMinutes()}] ${msg.name}: ${msg.message}\n`
            document.getElementById('chat').scrollTo(0, document.getElementById('chat').scrollHeight)
        } else {
            console.error('Error: ', event)
        }
    })
    socket.addEventListener('error', function (event, err) {
        window.alert('Error: ' + err)
    })
    socket.addEventListener('close', function (event) {
        window.alert('Connection closed')
    })
    document.getElementById('box').addEventListener('keydown', function (event) {
        if (event.key === 'Enter') {
            event.stopPropagation()
            event.preventDefault()
            sendChat()
        }
    })
    document.getElementById('chatbox').addEventListener('submit', function (event) {
        event.stopPropagation()
        event.preventDefault()
        sendChat()
    })
    function sendChat() {
        var message = document.getElementById('box').value
        if (message.length > 0) {
            socket.send(JSON.stringify({ id: me.id, name: me.name, message }))
            document.getElementById('box').value = ''
        }
    }
    function getColor() {
        var letters = 'BCDEF'.split(''),
            a = (new Array(6)).fill(0)
        return a.map(function () { return letters[Math.floor(Math.random() * letters.length)] }).join('')
    }
    function changeBgColor() {
        var color = getColor(),
            darkColor = ((('0x' + color) & 0xfefefe) >> 1).toString(16)
        document.body.style.backgroundColor = '#' + color
        document.getElementById('submit').style.color = '#' + color
        document.body.style.color = '#' + darkColor
        document.getElementById('chat').style.borderColor = '#' + darkColor
        document.getElementById('box').style.borderColor = '#' + darkColor
        document.getElementById('submit').style.background = '#' + darkColor
    }
    function startColor() {
        setTimeout(() => {
            changeBgColor()
            startColor()
        }, 30 * 1000)
    }
    document.getElementById('box').focus()
    changeBgColor()
    startColor()
})