const http = require('http'),
    JSONdb = require('simple-json-db'),
    express = require('express'),
    app = express(),
    server = http.createServer(app),
    WebSocket = require('ws'),
    db = new JSONdb('storage/db.json'),
    ws = new WebSocket.Server({ server }),
    isJSON = str => {
        try {
            JSON.parse(str)
        } catch (e) {
            return false
        }
        return true
    },
    config = require('./config'),
    auth = {
        login: config.username,
        password: config.password
    },
    userlist = []

ws.on('connection', connection = (wss, req) => {
    wss.on('message', message => {
        if (typeof message === 'object') {
            message = message.toString()
        }
        if (isJSON(message)) {
            const msg = JSON.parse(message),
                ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress
            if (msg.ping && Date.parse(new Date(msg.ping))) {
                wss.send(JSON.stringify({ pong: msg.ping }))
            } else if (msg.id && msg.name && !userlist.some(username => username === msg.name)) {
                if (db.has(msg.id)) {
                    const entry = db.get(msg.id)
                    if (!entry.name.some(username => username === msg.name)) {
                        entry.name.push(msg.name)
                    }
                    if (!entry.ip.some(i => i === ip)) {
                        entry.ip.push(ip)
                    }
                    entry.lastSeen = new Date()
                    entry.properties = msg.properties
                    db.set(msg.id, entry)
                } else {
                    db.set(msg.id, {
                        name: [msg.name],
                        ip: [ip],
                        lastSeen: new Date(),
                        properties: msg.properties
                    })
                }
                userlist.push(msg.name)
                wss.name = msg.name
                wss.id = msg.id
                ws.clients.forEach(client => {
                    if (client.readyState === WebSocket.OPEN) {
                        client.send(JSON.stringify({ userlist }))
                        client.send(JSON.stringify({ date: new Date(), name: msg.name, message: 'JOIN chat' }))
                    }
                })
                console.log('Connected %s: %s', msg.name, msg.id)
            } else if (msg.name && msg.message && userlist.some(username => username === msg.name)) {
                const mess = { date: new Date(), name: msg.name, message: msg.message }
                ws.clients.forEach(client => {
                    if (client.readyState === WebSocket.OPEN) {
                        client.send(JSON.stringify(mess), err => {
                            if (err) {
                                console.error('Error: ', err)
                            }
                        })
                    }
                })
                if (wss.id && db.has(wss.id)) {
                    const entry = db.get(wss.id)
                    if (entry.messages && Array.isArray(entry.messages)) {
                        entry.messages.push(mess)
                    } else {
                        entry.messages = [mess]
                    }
                    db.set(wss.id, entry)
                }
            } else {
                wss.close()
                console.error('Object not recognized: ', message.toString())
            }
        }
    })
    wss.on('close', () => {
        if (userlist.some(username => username === wss.name)) {
            if (db.has(wss.id)) {
                const entry = db.get(wss.id)
                entry.lastSeen = new Date()
                db.set(wss.id, entry)
            }
            userlist.splice(userlist.indexOf(wss.name), 1)
        }
        ws.clients.forEach(client => {
            if (client.readyState === WebSocket.OPEN) {
                client.send(JSON.stringify({ userlist }))
                client.send(JSON.stringify({ date: new Date(), name: wss.name, message: 'LEFT chat' }))
            }
        })
    })
})

app.disable('x-powered-by')
    .use(express.json({ extended: false }))
    .get('/db', (req, res) => {
        const b64auth = (req.headers.authorization || '').split(' ')[1] || '',
            [login, password] = Buffer.from(b64auth, 'base64').toString().split(':')
        if (login && password && login === auth.login && password === auth.password) {
            res.json(db.JSON())
        } else {
            res.set('WWW-Authenticate', 'Basic realm="401"')
            res.status(401).send('Authentication required.')
        }
    })
    .get('/api/userlist', (req, res) => {
        res.json(userlist)
    })
    .use(express.static('dist'))

server.listen(3000, () => {
    console.log(`Server running on: ${server.address().address}:${server.address().port}`)
})